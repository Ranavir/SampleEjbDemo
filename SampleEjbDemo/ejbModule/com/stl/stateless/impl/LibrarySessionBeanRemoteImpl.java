package com.stl.stateless.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.stl.stateless.LibrarySessionBeanLocal;
import com.stl.stateless.LibrarySessionBeanRemote;

/**
 * Session Bean implementation class LibrarySessionBean
 */
@Stateless
@Remote(LibrarySessionBeanRemote.class)
public class LibrarySessionBeanRemoteImpl implements  LibrarySessionBeanRemote{
	/*static InitialContext initialContext ;
	@EJB
	static LibrarySessionBeanLocal librarySessionBeanLocal ;
	static{
		
		
		try {
	    	  //initialContext = new InitialContext(props);
			initialContext = new InitialContext();
	    	  System.out.println("Context object created");
	    	  librarySessionBeanLocal=(LibrarySessionBeanLocal)initialContext.lookup("java:global/SampleEjbDemo/LibrarySessionBeanLocalImpl!com.stl.stateless.LibrarySessionBeanLocal") ;
		      System.out.println("LibrarySessionBeanLocalImpl object created");
	      } catch (NamingException ex) {
	      ex.printStackTrace();
	      }
		//memberLocalFacade=(MemberLocalFacade)initialContext.lookup("memberFacadeLocal");
	      //librarySessionBeanLocal=(LibrarySessionBeanLocal)initialContext.lookup("ejb:" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName) ;
	      //librarySessionBeanLocal=(LibrarySessionBeanLocal)initialContext.lookup("java:global/" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName) ;
		
	}*/
	//public InitialContext initialContext=null;
	@EJB
	public LibrarySessionBeanLocal librarySessionBeanLocal;
	/*public Properties props = null ;
	*//***************************//*
    final String appName = "";
    final String moduleName = "SampleEjbDemo";
    final String distinctName = "";
    final String beanName = LibrarySessionBeanLocalImpl.class.getSimpleName();
    final String viewClassName = LibrarySessionBeanLocal.class.getName();*/
    //return (RemoteCalculator) context.lookup("ejb:" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName);
    /**************************/
	public LibrarySessionBeanRemoteImpl() throws NamingException {
		System.out.println("Inside LibrarySessionBeanRemoteImpl Costructor");
			/*props = new Properties();
	      try {
	    	  File f = new File(this.getClass().getResource("resources/jndi.properties").getPath()) ;
	    	  System.out.println("Absolute path :"+f.getAbsolutePath());
	      props.load(new FileInputStream(f));
	      props.put(initialContext.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");//without this javax.naming.NameNotFoundException:
	      
	      } catch (IOException ex) {
	      ex.printStackTrace();
	      }*/
		/*try {
	    	  //initialContext = new InitialContext(props);
	    	  initialContext = new InitialContext();
	    	  System.out.println("Context object created");
	      } catch (NamingException ex) {
	      ex.printStackTrace();
	      }
		//memberLocalFacade=(MemberLocalFacade)initialContext.lookup("memberFacadeLocal");
	      //librarySessionBeanLocal=(LibrarySessionBeanLocal)initialContext.lookup("ejb:" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName) ;
	      //librarySessionBeanLocal=(LibrarySessionBeanLocal)initialContext.lookup("java:global/" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName) ;
	      librarySessionBeanLocal=(LibrarySessionBeanLocalImpl)initialContext.lookup("java:global/SampleEjbDemo/LibrarySessionBeanLocalImpl!com.stl.stateless.LibrarySessionBeanLocal") ;
	      System.out.println("LibrarySessionBeanLocalImpl object created");*/
	      
	      
    }
    
    /*public LibrarySessionBeanRemoteImpl(){
       bookShelf = new ArrayList<String>();
    }*/
    
    public void addBook(String bookName) {
    	System.out.println("Inside LibrarySessionBeanRemoteImpl addBook() "+bookName);
        //bookShelf.add(bookName);
    	librarySessionBeanLocal.addBook(bookName);
    }    
 
    public List<String> getBooks() {
    	System.out.println("Inside LibrarySessionBeanRemoteImpl getBooks() ");
        //return bookShelf;
    	return librarySessionBeanLocal.getBooks();
    }
}
