package com.stl.stateless.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.stl.stateless.LibrarySessionBeanLocal;
@Stateless
//@Local(LibrarySessionBeanLocal.class)
//@LocalBean
@LocalBean
public class LibrarySessionBeanLocalImpl implements LibrarySessionBeanLocal{
	List<String> bookShelf;    
    
    public LibrarySessionBeanLocalImpl(){
    	System.out.println("Inside LibrarySessionBeanLocalImpl Constructor");
       bookShelf = new ArrayList<String>();
    }
    
    public void addBook(String bookName) {
    	System.out.println("Inside LibrarySessionBeanLocalImpl addBook() "+bookName);
        bookShelf.add(bookName);
    }    
 
    public List<String> getBooks() {
    	System.out.println("Inside LibrarySessionBeanLocalImpl getBooks() ");
        return bookShelf;
    }
}
